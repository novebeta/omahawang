<?php
namespace app\controllers;
use app\models\Reservasi;
use app\models\ReservasiDetil;
use app\models\ReservasiSearch;
use app\models\Rooms;
use Yii;
use yii\base\UserException;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * ReservasiController implements the CRUD actions for Reservasi model.
 */
class ReservasiController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Reservasi models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new ReservasiSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}

	public function actionList( $q = null, $id = null ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$query                       = new Query;
		$out                         = [ 'results' => [ 'id' => '', 'text' => '' ] ];
		if ( ! is_null( $q ) ) {
			$query->select( 'reservasi.reservasi_id AS id,reservasi.doc_ref as text' )
			      ->from( 'reservasi' )
			      ->where( 'doc_ref LIKE "%' . $q . '%"' )
			      ->limit( 20 )
			      ->orderBy( 'doc_ref' );
			$command = $query->createCommand();
			$data    = $command->queryAll();
			$out['results'] = array_values( $data );
		} elseif ( $id > 0 ) {
			$out['results'] = [ 'id' => $id, 'text' => Reservasi::findOne( $id )->reservasi_id ];
		}
		return $out;
	}

	/**
	 * Displays a single Reservasi model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Displays a single Reservasi model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionViewAll() {
		return $this->render( 'viewall' );
	}
	public function actionEvent() {
		$query = new Query();
		$event = $query->select( "reservasi.reservasi_id AS id,
			(customers.nama||ota.nama) AS title,
			reservasi.arrival AS start,
			reservasi.depart AS end,
			reservasi_detil.room_id AS resourceId" )
		               ->from( "reservasi" )
		               ->innerJoin( 'reservasi_detil', 'reservasi_detil.reservasi_id = reservasi.reservasi_id' )
		               ->innerJoin( 'customers', 'reservasi.customer_id = customers.customer_id' )
		               ->innerJoin( 'ota', 'reservasi.ota_id = ota.ota_id' )
		               ->where( " ((reservasi.depart >= :start AND reservasi.depart <= :end) OR (reservasi.arrival >= :start AND reservasi.arrival <= :end))", [
			               ':start' => $_GET['start'],
			               ':end'   => $_GET['end'],
		               ] )
		               ->createCommand()
		               ->queryAll();
//		$event = [
//			[
//				'resourceId' => '4b76fb3f-e5c1-44be-838e-0f405c642d1a',
//				'id'         => 'd',
//				'start'      => '2019-08-06',
//				'end'        => '2019-08-08',
//				'title'      => 'event 3'
//			],
//		];
		return Json::encode( $event );
	}
	/**
	 * Creates a new Reservasi model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws UserException
	 */
	public function actionCreate() {
		$model = new Reservasi();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
			$details = json_decode( Yii::$app->request->post()['details'] );
			if ( $details != null ) {
				foreach ( $details as $row ) {
					$indo               = new ReservasiDetil();
					$indo->reservasi_id = $model->reservasi_id;
					$indo->room_id      = $row[0];
					$indo->qty          = $row[1];
					$indo->harga        = $row[2];
					$indo->total_line   = $row[3];
					$indo->note         = $row[4];
					if ( ! Rooms::checkAvailable( $indo->room_id, $model->arrival, $model->depart ) ) {
						throw new UserException( 'Kamar sudah dipesanan.' );
					}
					if ( ! $indo->save() ) {
						throw new UserException( Html::errorSummary( $model ) );
					}
				}
			}
			return $this->redirect( [ 'reservasi/index' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Reservasi model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws UserException
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
			$details = json_decode( Yii::$app->request->post()['details'] );
			if ( $details != null ) {
				foreach ( $details->body as $row ) {
					$indo               = new ReservasiDetil();
					$indo->reservasi_id = $model->reservasi_id;
					$indo->room_id      = $row[0];
					$indo->qty          = $row[1];
					$indo->harga        = $row[2];
					$indo->total_line   = $row[3];
					$indo->note         = $row[4];
					if ( ! Rooms::checkAvailable( $indo->room_id, $model->arrival, $model->depart ) ) {
						throw new UserException( 'Kamar sudah dipesanan.' );
					}
					if ( ! $indo->save() ) {
						throw new UserException( Html::errorSummary( $model ) );
					}
				}
			}
			return $this->redirect( [ 'reservasi/index' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Reservasi model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Reservasi model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Reservasi the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Reservasi::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
