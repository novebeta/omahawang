<?php
namespace app\controllers;
use app\models\Tjual;
use app\models\Tjuald;
use app\models\TjualSearch;
use Yii;
use yii\base\UserException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * TjualController implements the CRUD actions for Tjual model.
 */
class TjualController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Tjual models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new TjualSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Displays a single Tjual model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tjual model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tjual();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
			$details = json_decode( Yii::$app->request->post()['details'] );
			if ( $details != null ) {
				foreach ( $details as $row ) {
					$indo             = new Tjuald();
					$indo->tjual_id   = $model->tjual_id;
					$indo->tipe       = $row[0];
					$indo->item       = $row[1];
					$indo->qty        = $row[2];
					$indo->harga      = $row[3];
					$indo->total_line = $row[4];
					$indo->note       = $row[5];
					if ( ! $indo->save() ) {
						throw new UserException( Html::errorSummary( $model ) );
					}
				}
			}
			return $this->redirect( [ 'tjual/index' ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tjual model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
			$details = json_decode( Yii::$app->request->post()['details'] );
			Tjuald::deleteAll( [ 'tjual_id' => $id ] );
			if ( $details != null ) {
				foreach ( $details as $row ) {
					$indo             = new Tjuald();
					$indo->tjual_id   = $model->tjual_id;
					$indo->tipe       = $row[0];
					$indo->item       = $row[1];
					$indo->qty        = $row[2];
					$indo->harga      = $row[3];
					$indo->total_line = $row[4];
					$indo->note       = $row[5];
					if ( ! $indo->save() ) {
						throw new UserException( Html::errorSummary( $model ) );
					}
				}
			}
			return $this->redirect( [ 'tjual/index' ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Tjual model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Tjual model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tjual the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tjual::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
