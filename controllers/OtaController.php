<?php

namespace app\controllers;

use app\models\Customers;
use Yii;
use app\models\Ota;
use app\models\OtaSearch;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OtaController implements the CRUD actions for Ota model.
 */
class OtaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ota models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OtaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ota model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ota model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	public function actionCreate() {
		$model = new Ota();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
			return $this->redirect( [ 'view', 'id' => $model->ota_id ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}

    /**
     * Updates an existing Ota model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'update', [
					'model' => $model,
				] );
			}
			return $this->redirect( [ 'view', 'id' => $id ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}

    /**
     * Deletes an existing Ota model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ota model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ota the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ota::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
