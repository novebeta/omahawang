<?php
namespace app\controllers;
use app\models\Customers;
use app\models\Payment;
use app\models\PaymentSearch;
use Yii;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	/**
	 * Lists all Payment models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new PaymentSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Displays a single Payment model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Payment model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Payment();
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'create', [
					'model' => $model,
				] );
			}
			return $this->redirect( [ 'view', 'id' => $model->payment_id ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Payment model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( ! $model->save() ) {
				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'update', [
					'model' => $model,
				] );
			}
			return $this->redirect( [ 'view', 'id' => $id ] );
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}

	public function actionList( $q = null, $id = null ) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$query                       = new Query;
		$out                         = [ 'results' => [ 'id' => '', 'text' => '' ] ];
		if ( ! is_null( $q ) ) {
			$query->select( 'payment.payment_id AS id,payment.nama as text' )
			      ->from( 'payment' )
			      ->where( 'nama LIKE "%' . $q . '%"' )
			      ->limit( 20 )
			      ->orderBy( 'nama' );
			$command = $query->createCommand();
			$data    = $command->queryAll();
			$out['results'] = array_values( $data );
		} elseif ( $id > 0 ) {
			$out['results'] = [ 'id' => $id, 'text' => Payment::findOne( $id )->nama ];
		}
		return $out;
	}

	/**
	 * Deletes an existing Payment model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Payment model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Payment the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Payment::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
