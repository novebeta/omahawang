<?php
namespace app\assets;
use yii\web\AssetBundle;
class DataTablesAsset extends AssetBundle{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/dataTables.bootstrap.min.css',
		'css/buttons.bootstrap.min.css',
		'css/select.bootstrap.min.css',
		'css/editor.bootstrap.min.css',
		'css/shCore.css',
	];
	public $js = [
		'js/jquery.dataTables.min.js',
		'js/dataTables.bootstrap.min.js',
		'js/dataTables.buttons.min.js',
		'js/buttons.bootstrap.min.js',
		'js/dataTables.select.min.js',
		'js/sum().js',
//		'js/dataTables.editor.min.js',
//		'js/editor.bootstrap.min.js',
		'js/shCore.js',
	];
	public $depends = [
		'yii\web\JqueryAsset',
		'yii\bootstrap\BootstrapAsset'
	];
}