document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list', 'resourceTimeline'],
        now: '2019-08-01',//new Date().toISOString().slice(0,10),
        editable: true, // enable draggable events
        aspectRatio: 1.8,
        scrollTime: '00:00', // undo default 6am scrollTime
        header: {
            left: 'today prev,next',
            center: 'title',
            right: 'dayGridMonth,listWeek'
        },
        defaultView: 'dayGridMonth',
        resourceLabelText: 'Rooms',
        resources: resources,
        events: 'event'
    });
    calendar.render();
});