<?php
/* @var $this yii\web\View */
/* @var $model app\models\Rooms */
$this->title                   = 'Update Rooms: ' . $model->nama;
$this->params['breadcrumbs'][] = [ 'label' => 'Rooms', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->nama, 'url' => [ 'view', 'id' => $model->room_id ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rooms-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
