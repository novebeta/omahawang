<?php
use kartik\color\ColorInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Rooms */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-primary">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="form-group">
			<?= $form->field( $model, 'kode' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'nama' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'note' )->textInput( [ 'maxlength' => true ] ) ?>
        </div>
        <div class="form-group">
			<?= $form->field( $model, 'warna' )->widget(ColorInput::classname(), [
				'options' => ['placeholder' => 'Select color ...','class' => 'form-control' ],
            ] ) ?>
        </div>
        <div class="box-footer">
			<?= Html::submitButton( Yii::t( 'app', 'Save' ), [ 'class' => 'btn btn-success' ] ) ?>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
</div>