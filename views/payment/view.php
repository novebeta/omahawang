<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Payment */
$this->title                   = $model->nama;
$this->params['breadcrumbs'][] = [ 'label' => 'Payments', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register( $this );
?>
<div class="payment-view">
    <div class="box box-primary">
        <div class="box-header with-border">
			<?= Html::a( 'Update', [ 'update', 'id' => $model->payment_id ], [ 'class' => 'btn btn-primary' ] ) ?>
			<?= Html::a( 'Delete', [ 'delete', 'id' => $model->payment_id ], [
				'class' => 'btn btn-danger',
				'data'  => [
					'confirm' => 'Are you sure you want to delete this item?',
					'method'  => 'post',
				],
			] ) ?>
        </div>
        <div class="box-body">
			<?= DetailView::widget( [
				'model'      => $model,
				'attributes' => [
					'nama'
				],
			] ) ?>
        </div>
    </div>
</div>
