<?php
/* @var $this yii\web\View */
/* @var $model app\models\Payment */
$this->title                   = 'Update Payment: ' . $model->nama;
$this->params['breadcrumbs'][] = [ 'label' => 'Payments', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->nama, 'url' => [ 'view', 'id' => $model->payment_id ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
