<aside class="main-sidebar">
    <section class="sidebar">
		<?= dmstr\widgets\Menu::widget(
			[
				'options' => [ 'class' => 'sidebar-menu tree', 'data-widget' => 'tree' ],
				'items'   => app\modules\admin\components\MenuHelper::getAssignedMenu( Yii::$app->user->id )
			]
		) ?>
    </section>
</aside>
