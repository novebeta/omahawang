<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReservasiDetil */

$this->title = 'Create Reservasi Detil';
$this->params['breadcrumbs'][] = ['label' => 'Reservasi Detils', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservasi-detil-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
