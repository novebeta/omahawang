<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ReservasiDetilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reservasi Detils';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservasi-detil-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reservasi Detil', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'reservasi_detil_id',
            'reservasi_id',
            'room_id',
            'qty',
            'harga',
            //'total_line',
            //'note',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
