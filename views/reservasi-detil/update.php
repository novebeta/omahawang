<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReservasiDetil */

$this->title = 'Update Reservasi Detil: ' . $model->reservasi_detil_id;
$this->params['breadcrumbs'][] = ['label' => 'Reservasi Detils', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reservasi_detil_id, 'url' => ['view', 'id' => $model->reservasi_detil_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reservasi-detil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
