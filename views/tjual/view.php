<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tjual */

$this->title = $model->tjual_id;
$this->params['breadcrumbs'][] = ['label' => 'Tjuals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tjual-view">

	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>

</div>
