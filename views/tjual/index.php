<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TjualSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Penjualan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
	<?=
	\kartik\grid\GridView::widget(
		[
			"dataProvider"       => $dataProvider,
			'filterModel'        => $searchModel,
			"condensed"          => true,
			"hover"              => true,
			'headerRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
			'filterRowOptions'   => [ 'class' => 'kartik-sheet-style' ],
			'pjax'               => true,
			'toolbar'            => [
				[
					'content' =>
						Html::button( '<i class="glyphicon glyphicon-plus"></i>', [
							'type'    => 'button',
							'title'   => 'Add ' . $this->title,
							'class'   => 'btn btn-success',
							'onclick' => 'location.href="' . Url::to( [ '/tjual/create' ] ) . '"'
						] ) . ' ' .
						Html::a( '<i class="glyphicon glyphicon-repeat"></i>', [ 'index' ], [
							'data-pjax' => 0,
							'class'     => 'btn btn-default',
							'title'     => 'Reset Grid'
						] )
				],
				'{export}',
				'{toggleData}',
			],
			'export'             => [
				'fontAwesome' => true
			],
			'responsive'         => true,
			'floatHeader'        => true,
			'floatHeaderOptions' => [ 'scrollingTop' => true ],
			'showPageSummary'    => false,
			'toggleDataOptions'  => [ 'minCount' => 10 ],
			'panel'              => [
				'heading' => false,
				'footer'  => false,
			],
			"columns"            => [
				[
					'class'          => 'kartik\grid\SerialColumn',
					'contentOptions' => [ 'class' => 'kartik-sheet-style' ],
					'width'          => '36px',
					'header'         => '',
					'headerOptions'  => [ 'class' => 'kartik-sheet-style' ]
				],
				'doc_ref',
//				[
//					'label'     => 'Nama OTA',
//					'value'     => 'ota.nama'
//				],
				[
					'label'     => 'Nama Customer',
					'value'     => 'customer.nama'
				],
				'total',
				[ 'class' => 'yii\grid\ActionColumn' ],
			]
		]
	);
	?>
</div>