<?php
/* @var $this yii\web\View */
/* @var $model app\models\Tjual */
$this->title                   = 'Update Penjualan: ' . $model->doc_ref;
$this->params['breadcrumbs'][] = [ 'label' => 'Penjualan', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->doc_ref, 'url' => [ 'view', 'id' => $model->tjual_id ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tjual-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
