<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TjualSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tjual-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'tjual_id') ?>

    <?= $form->field($model, 'doc_ref') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'tgl') ?>

    <?= $form->field($model, 'tdate') ?>

    <?php // echo $form->field($model, 'tuser') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'reservasi_id') ?>

    <?php // echo $form->field($model, 'payment_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
