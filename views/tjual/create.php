<?php
/* @var $this yii\web\View */
/* @var $model app\models\Tjual */
$this->title                   = 'Create Sales';
$this->params['breadcrumbs'][] = [ 'label' => 'Sales', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tjual-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
