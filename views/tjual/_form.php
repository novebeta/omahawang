<?php
use app\models\Customers;
use app\models\Payment;
use app\models\Reservasi;
use app\models\Tjuald;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Tjual */
/* @var $form yii\widgets\ActiveForm */
app\assets\DataTablesAsset::register( $this );
$this->registerJsVar( 'dReservasi', $model->isNewRecord ? [] :
	array_map( 'array_values', Tjuald::find()
	                                 ->select( [ 'tipe', 'item', 'qty', 'harga', 'total_line', 'note' ] )
	                                 ->where( [ 'tjual_id' => $model->tjual_id ] )
	                                 ->asArray()
	                                 ->all() )
);
?>
    <div class="box box-primary">
		<?php $form = ActiveForm::begin( [ 'id' => 'tjual-form' ] ); ?>
        <div class="box-body">
            <div class="container-fluid"><!-- Row 1 -->
                <div class="col-lg-3">
                    <div class="form-group">
						<?= $form->field( $model, 'doc_ref' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
						<?= $form->field( $model, 'tgl' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								]
							]
						] ) ?>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
						<? echo $form->field( $model, 'reservasi_id' )->widget( Select2::classname(), [
							'initValueText' => ( $model->reservasi_id == null ) ? '' : Reservasi::findOne( $model->reservasi_id )->doc_ref,
//							'options'       => [ 'placeholder' => 'Search for a reservasi ...' ],
							'options'       => [ 'id' => 'reservasi-select2-id' ],
							'pluginOptions' => [
								'allowClear'   => true,
//		                        'minimumInputLength' => 3,
								'language'     => [ 'errorLoading' => new JsExpression( "function () { return 'Waiting for results...'; }" ), ],
								'ajax'         => [
									'url'      => Url::to( [ 'reservasi/list' ] ),
									'dataType' => 'json',
									'data'     => new JsExpression( 'function(params) { return {q:params.term}; }' )
								],
								'escapeMarkup' => new JsExpression( 'function (markup) { return markup; }' ),
								'pluginEvents' => [
									"select2:change" => "function() { console.log('change'); }"
								]
							],
						] );
						?>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
						<? echo $form->field( $model, 'customer_id' )->widget( Select2::classname(), [
							'initValueText' => ( $model->customer_id == null ) ? '' : Customers::findOne( $model->customer_id )->nama,
//							'options'       => [ 'placeholder' => 'Search for a customer ...' ],
							'options'       => [ 'id' => 'customers-select2-id' ],
							'pluginOptions' => [
								'allowClear'   => true,
//		                        'minimumInputLength' => 3,
								'language'     => [ 'errorLoading' => new JsExpression( "function () { return 'Waiting for results...'; }" ), ],
								'ajax'         => [
									'url'      => Url::to( [ 'customers/list' ] ),
									'dataType' => 'json',
									'data'     => new JsExpression( 'function(params) { return {q:params.term}; }' )
								],
								'escapeMarkup' => new JsExpression( 'function (markup) { return markup; }' ),
							],
						] );
						?>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-lg-12">
                    <div class="form-group">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Tipe</th>
                                <th>Item</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Note</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-lg-4">
                    <div class="form-group">
						<? echo $form->field( $model, 'payment_id' )->widget( Select2::classname(), [
							'initValueText' => ( $model->payment_id == null ) ? '' : Payment::findOne( $model->payment_id )->nama,
							'options'       => [ 'placeholder' => 'Search for a customer ...' ],
							'pluginOptions' => [
								'allowClear'   => true,
//		                        'minimumInputLength' => 3,
								'language'     => [ 'errorLoading' => new JsExpression( "function () { return 'Waiting for results...'; }" ), ],
								'ajax'         => [
									'url'      => Url::to( [ 'payment/list' ] ),
									'dataType' => 'json',
									'data'     => new JsExpression( 'function(params) { return {q:params.term}; }' )
								],
								'escapeMarkup' => new JsExpression( 'function (markup) { return markup; }' )
							],
						] );
						?>
                    </div>
                </div>
                <div class="col-lg-4">
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
						<?= $form->field( $model, 'total' )->widget( NumberControl::classname(), [
//						        'name'=> 'Tjual[total]',
							'id'                 => 'total_id',
							'maskedInputOptions' => [
								'allowMinus' => false
							],
							'value'              => $model->total,
							'readonly'           => true,
//							'disabled'           => true,
//				        'options' => $saveOptions,
//				        'displayOptions' => $dispOptions,
//				        'saveInputContainer' => $saveCont
						] );
						?>
                    </div>
                </div>
            </div>
            <div class="box-footer">
				<?= Html::submitButton( 'Save', [ 'class' => 'btn btn-success' ] ) ?>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
    <div class="modal fade" id="modal-pot">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detil</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <form id="form-medikal-bayar" class="form-horizontal">
                            <div class="form-group">
                                <label>Tipe</label>
								<?= Html::dropDownList( 'tipe-reservasi', null, [
									'UM' => 'LAINNYA',
									'KM' => 'KAMAR',
								], [ 'id' => 'tipe-reservasi', 'class' => 'form-control' ] ) ?>
                            </div>
                            <div class="form-group">
                                <label>Item</label>
								<?= Html::textInput( 'room-reservasi', '',
									[ 'id' => 'room-reservasi', 'class' => 'form-control' ] ) ?>
                            </div>
                            <div class="form-group">
                                <label for="qty-medikal-bayar">Qty</label>
								<?= NumberControl::widget( [
									'id'                 => "qty-reservasi",
									'name'               => "qty-reservasi",
									'maskedInputOptions' => [
										'allowMinus' => false
									],
									'options'            => [
									]
								] ); ?>
                            </div>
                            <div class="form-group">
                                <label>Harga</label>
								<?= NumberControl::widget( [
									'id'                 => "harga-reservasi",
									'name'               => "harga-reservasi",
									'maskedInputOptions' => [
										'prefix'     => 'Rp',
										'allowMinus' => false
									],
									'options'            => [
									]
								] ); ?>
                            </div>
                            <div class="form-group">
                                <label>Note</label>
                                <input type="text" class="form-control" id="note-reservasi">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-reservasi-add-row">Save changes
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
<?php
$this->registerJs( $this->render( 'script.js' ) );