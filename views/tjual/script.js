$(document).ready(function () {
    $('#example').DataTable({
        dom: 'Brtip',
        paging: false,
        select: true,
        columnDefs: [
            // {targets: [0], render: function (data, type, row, meta) {
            //     return cmbRooms[data];
            //     }},
            // {
            //     targets: [5, 8], render: function (data, type, row, meta) {
            //         return ((data == '1') ? '<img src="img/greencheck.png" width="15" alt="1" />' : '<img src="img/redx.png" width="15" alt="0" />');
            //     }
            // },
        ],
        buttons: [
            {
                text: 'Add Item',
                action: function (e, dt, node, config) {
                    var modal = $('#modal-pot');
                    modal.find('input').val('');
                    $('#updateId').val(null);
                    modal.modal();
                },
                className: 'dt-button'
            },
            {
                text: 'Edit Item',
                action: function (e, dt, node, config) {
                    var modal = $('#modal-pot');
                    var data = this.row('.selected').data();
                    $('#bulan-pot').val(data[0]);
                    $('#jml-pot').val(data[1]);
                    $('#updateId').val(this.row('.selected').index());
                    modal.modal();
                },
                className: 'dt-button'
            },
            {
                text: 'Delete Item',
                action: function (e, dt, node, config) {
                    this.row('.selected').remove().draw(false);
                },
                className: 'dt-button'
            }
        ]
    });
    $('#btn-reservasi-add-row').on('click', function () {
        var tipe = $('#tipe-reservasi').val();
        var id = $('#room-reservasi').val();
        var qty = $('#qty-reservasi').val();
        var harga = $('#harga-reservasi').val();
        var total = qty * harga;
        var note = $('#note-reservasi').val();
        var t = $('#example').DataTable();
        t.row.add([
            tipe,
            id,
            qty,
            harga,
            total,
            note
        ]).draw(false);
        $('#modal-pot').find('input').val('');
        let sum = t.column(4).data().sum();
        $('#tjual-total-disp').val(sum);
        $('#tjual-total').val(sum);
    });
    $('#tjual-form').submit(function () {
        // alert( "Handler for .submit() called." );
        // event.preventDefault();
        var dt_indo = $('#example').DataTable();
        // var indo = dt_indo.buttons.exportData();
        var riwayat_indo = JSON.stringify(dt_indo.rows().data().toArray());
        $("<input>").attr({
            'type': 'hidden',
            'name': 'details'
        }).val(riwayat_indo).appendTo(this);
    });
    var tIndo = $('#example').DataTable();
    tIndo.rows.add(dReservasi).draw();
    $('#reservasi-select2-id').on("select2:selecting", function(e) {
        console.log('test');
    });
});