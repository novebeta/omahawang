<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Customers */
$this->title                   = $model->nama;
$this->params['breadcrumbs'][] = [ 'label' => 'Customers', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $model->nama;
\yii\web\YiiAsset::register( $this );
?>
<div class="customers-view">
    <div class="box box-primary">
        <div class="box-header with-border">
			<?= Html::a( 'Update', [ 'update', 'id' => $model->customer_id ], [ 'class' => 'btn btn-primary' ] ) ?>
			<?= Html::a( 'Delete', [ 'delete', 'id' => $model->customer_id ], [
				'class' => 'btn btn-danger',
				'data'  => [
					'confirm' => 'Are you sure you want to delete this item?',
					'method'  => 'post',
				],
			] ) ?>
        </div>
        <div class="box-body">
			<?= DetailView::widget( [
				'model'      => $model,
				'attributes' => [
					'nama',
					'phone',
					'email:email',
					'alamat:ntext',
				],
			] ) ?>
        </div>
    </div>
</div>
