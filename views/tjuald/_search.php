<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TjualdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tjuald-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'tjuald_id') ?>

    <?= $form->field($model, 'tjual_id') ?>

    <?= $form->field($model, 'item') ?>

    <?= $form->field($model, 'harga') ?>

    <?= $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'total_line') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'tipe') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
