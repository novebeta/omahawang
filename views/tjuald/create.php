<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tjuald */

$this->title = 'Create Tjuald';
$this->params['breadcrumbs'][] = ['label' => 'Tjualds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tjuald-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
