<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tjuald */

$this->title = 'Update Tjuald: ' . $model->tjuald_id;
$this->params['breadcrumbs'][] = ['label' => 'Tjualds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tjuald_id, 'url' => ['view', 'id' => $model->tjuald_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tjuald-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
