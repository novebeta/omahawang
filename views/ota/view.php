<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Ota */
$this->title                   = $model->nama;
$this->params['breadcrumbs'][] = [ 'label' => 'Otas', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register( $this );
?>
<div class="ota-view">
    <div class="box box-primary">
        <div class="box-header with-border">
			<?= Html::a( 'Update', [ 'update', 'id' => $model->ota_id ], [ 'class' => 'btn btn-primary' ] ) ?>
			<?= Html::a( 'Delete', [ 'delete', 'id' => $model->ota_id ], [
				'class' => 'btn btn-danger',
				'data'  => [
					'confirm' => 'Are you sure you want to delete this item?',
					'method'  => 'post',
				],
			] ) ?>
        </div>
        <div class="box-body">
			<?= DetailView::widget( [
				'model'      => $model,
				'attributes' => [
					'nama',
				],
			] ) ?>
        </div>
    </div>
</div>
