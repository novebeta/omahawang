<?php
/* @var $this yii\web\View */
/* @var $model app\models\Ota */
$this->title                   = 'Update Ota: ' . $model->nama;
$this->params['breadcrumbs'][] = [ 'label' => 'Otas', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = [ 'label' => $model->nama, 'url' => [ 'view', 'id' => $model->ota_id ] ];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ota-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
