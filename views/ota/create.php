<?php
/* @var $this yii\web\View */
/* @var $model app\models\Ota */
$this->title                   = 'Create Ota';
$this->params['breadcrumbs'][] = [ 'label' => 'Otas', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ota-create">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
