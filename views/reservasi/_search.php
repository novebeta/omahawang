<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReservasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reservasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'reservasi_id') ?>

    <?= $form->field($model, 'doc_ref') ?>

    <?= $form->field($model, 'arrival') ?>

    <?= $form->field($model, 'depart') ?>

    <?= $form->field($model, 'check_in') ?>

    <?php // echo $form->field($model, 'check_out') ?>

    <?php // echo $form->field($model, 'customer_id') ?>

    <?php // echo $form->field($model, 'ota_id') ?>

    <?php // echo $form->field($model, 'canceled') ?>

    <?php // echo $form->field($model, 'pic_canceled') ?>

    <?php // echo $form->field($model, 'dp_') ?>

    <?php // echo $form->field($model, 'dp_payment_id') ?>

    <?php // echo $form->field($model, 'total') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
