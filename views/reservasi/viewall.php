<?php
/* @var $this yii\web\View */
use app\models\Rooms;
/* @var $model app\models\Reservasi */
//$this->title = $model->reservasi_id;
//$this->params['breadcrumbs'][] = ['label' => 'Reservasis', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//\yii\web\YiiAsset::register($this);
?>
<div class="reservasi-view">
    <div class="box box-primary">
        <div class="box-body">
            <div id='calendar'></div>
        </div>
    </div>
</div>
<?php
try {
    $this->registerJsVar('resources',Rooms::find()->calenderrsx());
	$this->registerCssFile( "@web/packages/core/main.css" );
	$this->registerCssFile( "@web/packages/daygrid/main.css" );
	$this->registerCssFile( "@web/packages/timegrid/main.css" );
	$this->registerCssFile( "@web/packages/list/main.css" );
	$this->registerCssFile( "@web/packages-premium/timeline/main.css" );
	$this->registerCssFile( "@web/packages-premium/resource-timeline/main.css" );
	$this->registerJsFile( "@web/packages/core/main.js" );
	$this->registerJsFile( "@web/packages/interaction/main.js" );
	$this->registerJsFile( "@web/packages/daygrid/main.js" );
	$this->registerJsFile( "@web/packages/timegrid/main.js" );
	$this->registerJsFile( "@web/packages/list/main.js" );
	$this->registerJsFile( "@web/packages-premium/timeline/main.js" );
	$this->registerJsFile( "@web/packages-premium/resource-common/main.js" );
	$this->registerJsFile( "@web/packages-premium/resource-timeline/main.js" );
	$this->registerJsFile( "@web/js/viewall.js" );
} catch ( \yii\base\InvalidConfigException $e ) {
}
?>

