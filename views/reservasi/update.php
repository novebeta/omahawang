<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reservasi */

$this->title = 'Update Pesanan: ' . $model->doc_ref;
$this->params['breadcrumbs'][] = ['label' => 'Pesanan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->doc_ref, 'url' => ['view', 'id' => $model->reservasi_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reservasi-update">


    <?= $this->render('_form', [
        'model' => $model,
        'mode'=> 'update'
    ]) ?>

</div>
