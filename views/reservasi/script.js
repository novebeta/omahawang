$(document).ready(function () {
    $('#example').DataTable({
        dom: 'Brtip',
        paging: false,
        select: true,
        columnDefs: [
            {
                targets: [0], render: function (data, type, row, meta) {
                    return cmbRooms[data];
                }
            },
            // {
            //     targets: [5, 8], render: function (data, type, row, meta) {
            //         return ((data == '1') ? '<img src="img/greencheck.png" width="15" alt="1" />' : '<img src="img/redx.png" width="15" alt="0" />');
            //     }
            // },
        ],
        buttons: [
            {
                text: 'Add Item',
                action: function (e, dt, node, config) {
                    var modal = $('#modal-pot');
                    modal.find('input').val('');
                    var arrival = $('#reservasi-arrival').val();
                    var depart = $('#reservasi-depart').val();
                    if (arrival == '' || depart == '') {
                        alert('Tgl arrival dan depart tidak boleh kosong.');
                    } else {
                        modal.modal();
                        let tglA = Date.parse(arrival);
                        let tglD = Date.parse(depart);
                        const diffTime = Math.abs(tglD - tglA);
                        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                        $('#qty-reservasi-disp').val(diffDays);
                    }
                },
                className: 'dt-button'
            },
            {
                text: 'Edit Item',
                action: function (e, dt, node, config) {
                    var modal = $('#modal-pot');
                    var data = this.row('.selected').data();
                    $('#bulan-pot').val(data[0]);
                    $('#jml-pot').val(data[1]);
                    $('#updateId').val(this.row('.selected').index());
                    modal.modal();
                },
                className: 'dt-button'
            },
            {
                text: 'Delete Item',
                action: function (e, dt, node, config) {
                    this.row('.selected').remove().draw(false);
                },
                className: 'dt-button'
            }
        ]
    });
    $('#btn-reservasi-add-row').on('click', function () {
        var id = $('#room-reservasi').val();
        var qty = $('#qty-reservasi-disp').val();
        var harga = $('#harga-reservasi').val();
        var total = qty * harga;
        var note = $('#note-reservasi').val();
        var t = $('#example').DataTable();
        t.row.add([
            id,
            qty,
            harga,
            total,
            note
        ]).draw(false);
        $('#harga-reservasi').val('');
        $('#note-reservasi').val('');
    });
    $('#reservasi-form').submit(function () {
        // alert( "Handler for .submit() called." );
        // event.preventDefault();
        var dt_indo = $('#example').DataTable();
        // var indo = dt_indo.buttons.exportData();
        var riwayat_indo = JSON.stringify(dt_indo.rows().data().toArray());
        $("<input>").attr({
            'type': 'hidden',
            'name': 'details'
        }).val(riwayat_indo).appendTo(this);
    });
    var tIndo = $('#example').DataTable();
    tIndo.rows.add(dReservasi).draw();
});