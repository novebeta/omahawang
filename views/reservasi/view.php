<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Reservasi */

$this->title = $model->reservasi_id;
$this->params['breadcrumbs'][] = ['label' => 'Reservasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="reservasi-view">


	<?= $this->render('_form', [
		'model' => $model,
		'mode'=> 'view'
	]) ?>

</div>
