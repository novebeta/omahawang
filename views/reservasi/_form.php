<?php
use app\models\Customers;
use app\models\Ota;
use app\models\ReservasiDetil;
use app\models\Rooms;
use kartik\datecontrol\DateControl;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Reservasi */
/* @var $form yii\widgets\ActiveForm */
app\assets\DataTablesAsset::register( $this );
$cmbRooms = ArrayHelper::map( Rooms::find()->combo(), 'id', 'title' );
$this->registerJsVar( 'cmbRooms', $cmbRooms );
$this->registerJsVar( 'dReservasi', $model->isNewRecord ? [] :
	array_map( 'array_values', ReservasiDetil::find()
	                                         ->select( [ 'room_id', 'qty', 'harga', 'total_line', 'note' ] )
	                                         ->where( [ 'reservasi_id' => $model->reservasi_id ] )
	                                         ->asArray()
	                                         ->all() )
);
?>
    <div class="box box-primary">
		<?php $form = ActiveForm::begin( [ 'id' => 'reservasi-form' ] ); ?>
        <div class="box-body">
            <div class="container-fluid"><!-- Row 1 -->
                <div class="col-lg-4">
                    <div class="form-group">
						<?= $form->field( $model, 'doc_ref' )->textInput( [ 'maxlength' => true ] ) ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
						<?= $form->field( $model, 'tgl' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'widgetOptions'  => [
								'pluginOptions' => [
									'autoclose' => true
								]
							]
						] ) ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
						<? echo $form->field( $model, 'customer_id' )->widget( Select2::classname(), [
							'initValueText' => $model->isNewRecord ? '' : Customers::findOne( $model->customer_id )->nama,
							'options'       => [ 'placeholder' => 'Search for a customer ...' ],
							'pluginOptions' => [
								'allowClear'   => true,
//		                        'minimumInputLength' => 3,
								'language'     => [ 'errorLoading' => new JsExpression( "function () { return 'Waiting for results...'; }" ), ],
								'ajax'         => [
									'url'      => Url::to( [ 'customers/list' ] ),
									'dataType' => 'json',
									'data'     => new JsExpression( 'function(params) { return {q:params.term}; }' )
								],
								'escapeMarkup' => new JsExpression( 'function (markup) { return markup; }' ),
//		                        'templateResult'     => new JsExpression( 'repoFormatResult' ),
//		                        'templateSelection'  => new JsExpression( 'repoFormatSelection' ),
							],
						] );
						?>
                    </div>
                </div>
            </div>
            <div class="container-fluid"><!-- Row 1 -->
                <div class="col-lg-4">
                    <div class="form-group">
						<?= $form->field( $model, 'ota_id' )->dropDownList(
							ArrayHelper::map( Ota::find()->select( [
								'ota_id',
								'nama'
							] )->asArray( true )->all(), 'ota_id', 'nama'
							),
							[ 'prompt' => '' ] ) ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
						<?= $form->field( $model, 'arrival' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'widgetOptions'  => [ 'pluginOptions' => [ 'autoclose' => true ] ]
						] ) ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
						<?= $form->field( $model, 'depart' )->widget( DateControl::classname(), [
							'type'           => DateControl::FORMAT_DATE,
							'ajaxConversion' => false,
							'widgetOptions'  => [ 'pluginOptions' => [ 'autoclose' => true ] ]
						] ) ?>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="box-body">
                    <table id="example" class="table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Room</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th>Total</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
				<?= Html::submitButton( 'Save', [ 'class' => 'btn btn-success' ] ) ?>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
    <div class="modal fade" id="modal-pot">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detil</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <form id="form-medikal-bayar" class="form-horizontal">
                            <div class="form-group">
                                <label>Room</label>
								<?= Html::dropDownList( 'room-reservasi', null, ArrayHelper::map( Rooms::find()->combo(), 'id', 'title' ),
									[ 'id' => 'room-reservasi', 'class' => 'form-control' ] ) ?>
                            </div>
                            <div class="form-group">
                                <label for="qty-medikal-bayar">Qty</label>
								<?= NumberControl::widget( [
									'disabled'           => true,
									'id'                 => "qty-reservasi",
									'name'               => "qty-reservasi",
									'maskedInputOptions' => [
										'allowMinus' => false
									],
									'options'            => [
									]
								] ); ?>
                            </div>
                            <div class="form-group">
                                <label>Harga</label>
								<?= NumberControl::widget( [
									'id'                 => "harga-reservasi",
									'name'               => "harga-reservasi",
									'maskedInputOptions' => [
										'prefix'     => 'Rp',
										'allowMinus' => false
									],
									'options'            => [
									]
								] ); ?>
                            </div>
                            <div class="form-group">
                                <label>Note</label>
                                <input type="text" class="form-control" id="note-reservasi">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-reservasi-add-row">Save changes
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- /.modal-dialog -->
<?php
$this->registerJs( $this->render( 'script.js' ) );