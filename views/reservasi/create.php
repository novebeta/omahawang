<?php
/* @var $this yii\web\View */
/* @var $model app\models\Reservasi */
$this->title                   = 'Create Reservasi';
$this->params['breadcrumbs'][] = [ 'label' => 'Reservasi', 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservasi-create">
	<?= $this->render( '_form', [
		'model' => $model,
        'mode'=> 'create'
	] ) ?>
</div>
