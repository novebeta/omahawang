<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\modules\admin\models\searchs\Menu */
$this->title                   = Yii::t( 'rbac-admin', 'Menus' );
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">
    <div class="box box-primary">
        <div class="box-header with-border">
			<?= Html::a( Yii::t( 'rbac-admin', 'Create Menu' ), [ 'create' ], [ 'class' => 'btn btn-success' ] ) ?>
        </div>
        <div class="box-body">
			<?php Pjax::begin(); ?>
			<?=
			GridView::widget( [
				'dataProvider' => $dataProvider,
				'filterModel'  => $searchModel,
				'columns'      => [
					[ 'class' => 'yii\grid\SerialColumn' ],
					'name',
					[
						'attribute' => 'menuParent.name',
						'filter'    => Html::activeTextInput( $searchModel, 'parent_name', [
							'class' => 'form-control',
							'id'    => null
						] ),
						'label'     => Yii::t( 'rbac-admin', 'Parent' ),
					],
					'route',
					'order',
					[ 'class' => 'yii\grid\ActionColumn' ],
				],
			] );
			?>
			<?php Pjax::end(); ?>
        </div>
    </div>
</div>
