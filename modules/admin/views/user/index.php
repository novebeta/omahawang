<?php
use yii\grid\GridView;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Yii::t( 'rbac-admin', 'Users' );
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div class="box box-primary">
        <div class="box-header with-border">
			<?= Html::a( Yii::t( 'app', 'Create User' ), [ 'signup' ], [ 'class' => 'btn btn-success' ] ) ?>
        </div>
        <div class="box-body">
			<?=
			GridView::widget( [
				'dataProvider' => $dataProvider,
				'filterModel'  => $searchModel,
				'columns'      => [
					[ 'class' => 'yii\grid\SerialColumn' ],
					'username',
					'email:email',
					[
						'attribute' => 'status',
						'value'     => function ( $model ) {
							return $model->status == 0 ? 'Inactive' : 'Active';
						},
						'filter'    => [
							0  => 'Inactive',
							10 => 'Active'
						]
					],
					[
						'class'    => 'yii\grid\ActionColumn',
						'template' => '{view} {update}'
					],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'template' => Helper::filterActionColumn(['view','update', 'activate', 'delete']),
//                'buttons' => [
//                    'activate' => function($url, $model) {
//                        if ($model->status == 10) {
//                            return '';
//                        }
//                        $options = [
//                            'title' => Yii::t('rbac-admin', 'Activate'),
//                            'aria-label' => Yii::t('rbac-admin', 'Activate'),
//                            'data-confirm' => Yii::t('rbac-admin', 'Are you sure you want to activate this user?'),
//                            'data-method' => 'post',
//                            'data-pjax' => '0',
//                        ];
//                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, $options);
//                    }
//                    ]
//                ],
				],
			] );
			?>
        </div>
    </div>
</div>
