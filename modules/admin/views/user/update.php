<?php
/* @var $this yii\web\View */
/* @var $model app\models\UsersBase */
$this->title                   = $model->username;
$this->params['breadcrumbs'][] = [ 'label' => Yii::t( 'rbac-admin', 'Users' ), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-update">
	<?= $this->render( '_form', [
		'model' => $model,
	] ) ?>
</div>
