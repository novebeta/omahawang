<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
				<?php $form = ActiveForm::begin(); ?>
                <div class="box-body">
                    <div class="box-footer">
						<?= Html::submitButton( $model->isNewRecord ? Yii::t( 'rbac-admin', 'Create' ) : Yii::t( 'rbac-admin', 'Update' ), [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ] ) ?>
                    </div>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
