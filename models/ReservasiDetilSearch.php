<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReservasiDetil;

/**
 * ReservasiDetilSearch represents the model behind the search form of `app\models\ReservasiDetil`.
 */
class ReservasiDetilSearch extends ReservasiDetil
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reservasi_detil_id', 'reservasi_id', 'room_id', 'note'], 'safe'],
            [['qty'], 'integer'],
            [['harga', 'total_line'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReservasiDetil::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'qty' => $this->qty,
            'harga' => $this->harga,
            'total_line' => $this->total_line,
        ]);

        $query->andFilterWhere(['like', 'reservasi_detil_id', $this->reservasi_detil_id])
            ->andFilterWhere(['like', 'reservasi_id', $this->reservasi_id])
            ->andFilterWhere(['like', 'room_id', $this->room_id])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
