<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reservasi".
 *
 * @property string $reservasi_id
 * @property string $doc_ref
 * @property string $arrival
 * @property string $depart
 * @property string $check_in
 * @property string $check_out
 * @property string $customer_id
 * @property string $ota_id
 * @property string $canceled
 * @property string $pic_canceled
 * @property string $total
 * @property string $dp_ [decimal(15)]
 * @property string $dp_payment_id [varchar(36)]
 */
class Reservasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reservasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['reservasi_id'], 'default', 'value' => \thamtech\uuid\helpers\UuidHelper::uuid()],
            [['reservasi_id', 'doc_ref', 'arrival', 'depart'], 'required'],
            [['tgl','arrival', 'depart', 'check_in', 'check_out', 'canceled'], 'safe'],
            [['total'], 'number'],
            [['reservasi_id', 'customer_id', 'ota_id'], 'string', 'max' => 36],
            [['doc_ref'], 'string', 'max' => 20],
            [['pic_canceled'], 'string', 'max' => 100],
            [['reservasi_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reservasi_id' => 'Reservasi ID',
            'doc_ref' => 'Doc Ref',
            'tgl' => 'Tgl',
            'arrival' => 'Arrival',
            'depart' => 'Depart',
            'check_in' => 'Check In',
            'check_out' => 'Check Out',
            'customer_id' => 'Customer ID',
            'ota_id' => 'Ota ID',
            'canceled' => 'Canceled',
            'pic_canceled' => 'Pic Canceled',
            'total' => 'Total',
        ];
    }

	public function getOta() {
		return $this->hasOne( Ota::className(), [ 'ota_id' => 'ota_id' ] );
	}
	public function getCustomer() {
		return $this->hasOne( Customers::className(), [ 'customer_id' => 'customer_id' ] );
	}
}
