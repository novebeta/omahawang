<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tjuald;

/**
 * TjualdSearch represents the model behind the search form of `app\models\Tjuald`.
 */
class TjualdSearch extends Tjuald
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tjuald_id', 'tjual_id', 'item', 'note', 'tipe'], 'safe'],
            [['harga', 'total_line'], 'number'],
            [['qty'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tjuald::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'harga' => $this->harga,
            'qty' => $this->qty,
            'total_line' => $this->total_line,
        ]);

        $query->andFilterWhere(['like', 'tjuald_id', $this->tjuald_id])
            ->andFilterWhere(['like', 'tjual_id', $this->tjual_id])
            ->andFilterWhere(['like', 'item', $this->item])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'tipe', $this->tipe]);

        return $dataProvider;
    }
}
