<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tjual;

/**
 * TjualSearch represents the model behind the search form of `app\models\Tjual`.
 */
class TjualSearch extends Tjual
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tjual_id', 'doc_ref', 'customer_id', 'tgl', 'tdate', 'tuser', 'reservasi_id', 'payment_id'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tjual::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tgl' => $this->tgl,
            'tdate' => $this->tdate,
            'total' => $this->total,
        ]);

        $query->andFilterWhere(['like', 'tjual_id', $this->tjual_id])
            ->andFilterWhere(['like', 'doc_ref', $this->doc_ref])
            ->andFilterWhere(['like', 'customer_id', $this->customer_id])
            ->andFilterWhere(['like', 'tuser', $this->tuser])
            ->andFilterWhere(['like', 'reservasi_id', $this->reservasi_id])
            ->andFilterWhere(['like', 'payment_id', $this->payment_id]);

        return $dataProvider;
    }
}
