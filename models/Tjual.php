<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tjual".
 *
 * @property string $tjual_id
 * @property string $doc_ref
 * @property string $customer_id
 * @property string $tgl
 * @property string $tdate
 * @property string $tuser
 * @property string $total
 * @property string $reservasi_id
 * @property string $payment_id
 */
class Tjual extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tjual';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        [['tjual_id'], 'default', 'value' => \thamtech\uuid\helpers\UuidHelper::uuid()],
            [['tjual_id'], 'required'],
            [['tgl', 'tdate'], 'safe'],
            [['total'], 'number'],
            [['tjual_id', 'customer_id', 'tuser', 'reservasi_id', 'payment_id'], 'string', 'max' => 36],
            [['doc_ref'], 'string', 'max' => 20],
            [['tjual_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tjual_id' => 'Tjual ID',
            'doc_ref' => 'Doc Ref',
            'customer_id' => 'Customer ID',
            'tgl' => 'Tgl',
            'tdate' => 'Tdate',
            'tuser' => 'Tuser',
            'total' => 'Total',
            'reservasi_id' => 'Reservasi ID',
            'payment_id' => 'Payment ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TjualQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TjualQuery(get_called_class());
    }


	public function getOta() {
		return $this->hasOne( Ota::className(), [ 'ota_id' => 'ota_id' ] );
	}
	public function getCustomer() {
		return $this->hasOne( Customers::className(), [ 'customer_id' => 'customer_id' ] );
	}
}
