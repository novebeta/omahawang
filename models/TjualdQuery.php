<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Tjuald]].
 *
 * @see Tjuald
 */
class TjualdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tjuald[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tjuald|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
