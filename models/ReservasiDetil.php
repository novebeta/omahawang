<?php
namespace app\models;
use yii\db\Query;
/**
 * This is the model class for table "reservasi_detil".
 *
 * @property string $reservasi_detil_id
 * @property string $reservasi_id
 * @property string $room_id
 * @property int $qty
 * @property string $harga
 * @property string $total_line
 * @property string $note
 */
class ReservasiDetil extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'reservasi_detil';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'reservasi_detil_id' ], 'default', 'value' => \thamtech\uuid\helpers\UuidHelper::uuid() ],
			[ [ 'reservasi_detil_id' ], 'required' ],
			[ [ 'qty' ], 'integer' ],
			[ [ 'harga', 'total_line' ], 'number' ],
			[ [ 'reservasi_detil_id', 'reservasi_id', 'room_id' ], 'string', 'max' => 36 ],
			[ [ 'note' ], 'string', 'max' => 600 ],
			[ [ 'reservasi_detil_id' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'reservasi_detil_id' => 'Reservasi Detil ID',
			'reservasi_id'       => 'Reservasi ID',
			'room_id'            => 'Room ID',
			'qty'                => 'Qty',
			'harga'              => 'Harga',
			'total_line'         => 'Total Line',
			'note'               => 'Note',
		];
	}
}
