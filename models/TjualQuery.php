<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Tjual]].
 *
 * @see Tjual
 */
class TjualQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Tjual[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Tjual|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
