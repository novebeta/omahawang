<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property string $payment_id
 * @property string $nama
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        [['payment_id'], 'default', 'value' => \thamtech\uuid\helpers\UuidHelper::uuid()],
            [['payment_id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 100],
            [['payment_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'payment_id' => 'Payment ID',
            'nama' => 'Nama',
        ];
    }
}
