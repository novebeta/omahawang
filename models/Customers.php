<?php

namespace app\models;

use Yii;
use yii\db\Expression;
/**
 * This is the model class for table "customers".
 *
 * @property string $customer_id
 * @property string $nama
 * @property string $phone
 * @property string $email
 * @property string $alamat
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'default', 'value' => \thamtech\uuid\helpers\UuidHelper::uuid()],
            [['alamat'], 'string'],
            [['customer_id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
            [['email'], 'email'],
            [['customer_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'nama' => 'Nama',
            'phone' => 'Phone',
            'email' => 'Email',
            'alamat' => 'Alamat',
        ];
    }
}
