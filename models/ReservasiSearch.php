<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reservasi;

/**
 * ReservasiSearch represents the model behind the search form of `app\models\Reservasi`.
 */
class ReservasiSearch extends Reservasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reservasi_id', 'doc_ref', 'arrival', 'depart', 'check_in', 'check_out', 'customer_id', 'ota_id', 'canceled', 'pic_canceled'], 'safe'],
            [[ 'total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reservasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'arrival' => $this->arrival,
            'depart' => $this->depart,
            'check_in' => $this->check_in,
            'check_out' => $this->check_out,
            'canceled' => $this->canceled,
            'total' => $this->total,
        ]);

        $query->andFilterWhere(['like', 'reservasi_id', $this->reservasi_id])
            ->andFilterWhere(['like', 'doc_ref', $this->doc_ref])
            ->andFilterWhere(['like', 'customer_id', $this->customer_id])
            ->andFilterWhere(['like', 'ota_id', $this->ota_id])
            ->andFilterWhere(['like', 'pic_canceled', $this->pic_canceled]);

        return $dataProvider;
    }
}
