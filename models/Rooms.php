<?php

namespace app\models;

use Yii;
use yii\db\Query;
/**
 * This is the model class for table "rooms".
 *
 * @property string $room_id
 * @property string $kode
 * @property string $nama
 * @property string $note
 * @property string $warna
 */
class Rooms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rooms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        [['room_id'], 'default', 'value' => \thamtech\uuid\helpers\UuidHelper::uuid()],
            [['room_id'], 'required'],
            [['room_id'], 'string', 'max' => 36],
            [['kode', 'warna'], 'string', 'max' => 50],
            [['nama'], 'string', 'max' => 100],
            [['note'], 'string', 'max' => 600],
            [['room_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room ID',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'note' => 'Note',
            'warna' => 'Warna',
        ];
    }


	public static function checkAvailable( $room_id, $arrival, $depart ) {
		$query = new Query();
		$event = $query->select( "reservasi.reservasi_id" )
		               ->from( "reservasi" )
		               ->innerJoin( 'reservasi_detil', 'reservasi_detil.reservasi_id = reservasi.reservasi_id' )
		               ->where( " ((reservasi.depart >= :start AND reservasi.depart <= :end) 
		               OR (reservasi.arrival >= :start AND reservasi.arrival <= :end)) AND reservasi_detil.room_id = :room_id", [
			               ':start'   => $arrival,
			               ':end'     => $depart,
			               ':room_id' => $room_id
		               ] )
		               ->createCommand()
		               ->queryAll();
		return sizeof( $event ) == 0;
	}

    /**
     * {@inheritdoc}
     * @return RoomsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RoomsQuery(get_called_class());
    }
}
