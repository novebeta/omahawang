<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tjuald".
 *
 * @property string $tjuald_id
 * @property string $tjual_id
 * @property string $item
 * @property string $harga
 * @property int $qty
 * @property string $total_line
 * @property string $note
 * @property string $tipe
 */
class Tjuald extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tjuald';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        [['tjuald_id'], 'default', 'value' => \thamtech\uuid\helpers\UuidHelper::uuid()],
            [['tjuald_id'], 'required'],
            [['harga', 'total_line'], 'number'],
            [['qty'], 'integer'],
            [['tjuald_id', 'tjual_id'], 'string', 'max' => 36],
            [['item'], 'string', 'max' => 50],
            [['note'], 'string', 'max' => 600],
            [['tipe'], 'string', 'max' => 2],
            [['tjuald_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tjuald_id' => 'Tjuald ID',
            'tjual_id' => 'Tjual ID',
            'item' => 'Item',
            'harga' => 'Harga',
            'qty' => 'Qty',
            'total_line' => 'Total Line',
            'note' => 'Note',
            'tipe' => 'Tipe',
        ];
    }

    /**
     * {@inheritdoc}
     * @return TjualdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TjualdQuery(get_called_class());
    }
}
