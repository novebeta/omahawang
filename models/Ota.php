<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ota".
 *
 * @property string $ota_id
 * @property string $nama
 */
class Ota extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ota';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        [['ota_id'], 'default', 'value' => \thamtech\uuid\helpers\UuidHelper::uuid()],
            [['ota_id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 100],
            [['ota_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ota_id' => 'Ota ID',
            'nama' => 'Nama',
        ];
    }
}
